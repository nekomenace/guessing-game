use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {

    let mut turns: u32 = 0;

    println!("Guess the number!");

    let secret_num: u32 = rand::thread_rng().gen_range(1..=100);
    #[cfg(debug_assertions)] println!("[DEBUG] The secret number is {}.", secret_num);

    loop {

        turns += 1;

        println!("Enter your guess..");

        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read input :(");

        let guess: u32 = match guess
                                .trim()
                                .parse() {
                                    Ok(num) => num,
                                    Err(_) => {
                                        println!("Enter numbers only!");
                                        continue;
                                    }
                                };

        match guess.cmp(&secret_num) {
            Ordering::Less => println!("Too Small!"),
            Ordering::Equal => {
                println!("Bingo!🐱\nYou got it right! And it took you only {} turns.", turns);
                break;
            },
            Ordering::Greater => println!("Too big!"),
        }
    }
}