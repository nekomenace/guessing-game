# Guessing Game
This is the same Guessing game that was in RustBook.

## Building

To build in debug mode, do
```
cargo build
```

To build in release mode, do
```
cargo build --release
```

The application to run is found in `target/debug/` or `target/release`.